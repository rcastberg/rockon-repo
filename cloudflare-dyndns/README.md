# Docker cloudflare updater
Based on benkulbertis/cloudflare-update-record.sh

```
docker run \
-e auth_email=test@example.com
-e auth_key=c3248672abe452387245cdfff238478921774996205bc
-e zone_name=example.com
-e record_name=www.example.com
-e TIMER=600 \
rcastberg/cloudflare_ip
```


Or use it in your `docker-compose.yml` file:

```yaml
cloudflare_ip:
    build: rcastberg/cloudflare_ip
    environment:
	auth_email : test@example.com
	auth_key : c3248672abe452387245cdfff238478921774996205bc
	zone_name : example.com
	record_name : www.example.com
        TIMER: 600
```
