#! /bin/bash

#Update cloudflare ip address
# Requires :
#   $auth_email as the authorized email e.g. test@example.com
#   $auth_key as the authorized key from cloudflare settings
#               e.g. c3248672abe452387245cdfff238478921774996205bc
#   $zone_name the name of the zone, e.g. example.com
#   $record_name , e.g. www.example.com
#   $TIMER, how often to run this loop in s, 3600
log() {
    if [ "$1" ]; then
        echo -e "[$(date)] - $1" >> $log_file
    fi
}

ip_file="/data/ip.txt"
id_file="/data/cloudflare.ids"
log_file="/data/cloudflare.log"

log "Check Initiated"

while [ true ]; do
    ip=`dig +short myip.opendns.com @resolver1.opendns.com`
    #Read old IP
    if [ -f $ip_file ]; then
        old_ip=$(cat $ip_file)
        if [ $ip == $old_ip ]; then
            echo "IP has not changed."
            exit 0
        fi
    fi

    #Get zone and record identifier
    if [ -f $id_file ] && [ $(wc -l $id_file | cut -d " " -f 1) == 2 ]; then
        zone_identifier=$(head -1 $id_file)
        record_identifier=$(tail -1 $id_file)
    else
        zone_identifier=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones?name=$zone_name" -H "X-Auth-Email: $auth_email" -H "X-Auth-Key: $auth_key" -H "Content-Type: application/json" |tr ',' '\n'|tr '{' '\n'|grep \"id\"|cut -f 2 -d ':'|sed s/\"//g | head -1 )
        record_identifier=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$zone_identifier/dns_records?name=$record_name" -H "X-Auth-Email: $auth_email" -H "X-Auth-Key: $auth_key" -H "Content-Type: application/json"  | tr ',' '\n'|tr '{' '\n'|grep \"id\"|cut -f 2 -d ':'|sed s/\"//g )
        echo "$zone_identifier" > $id_file
        echo "$record_identifier" >> $id_file
    fi

    update=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$zone_identifier/dns_records/$record_identifier" -H "X-Auth-Email: $auth_email" -H "X-Auth-Key: $auth_key" -H "Content-Type: application/json" --data "{\"id\":\"$zone_identifier\",\"type\":\"A\",\"name\":\"$record_name\",\"content\":\"$ip\"}")

    #Log output to logfile
    if [[ $update == *"\"success\":false"* ]]; then
        message="API UPDATE FAILED. DUMPING RESULTS:\n$update"
        log "$message"
        echo -e "$message"
        exit 1 
    else
        message="IP changed to: $ip"
        echo "$ip" > $ip_file
        log "$message"
        echo "$message"
    fi

    sleep $TIMER
done

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
